function clicked() {
    let f1 = document.getElementsByName("field1");
    let f2 = document.getElementsByName("field2");
    let r = document.getElementById("result");
    if (!(/^[0-9]+$/).test(f1[0].value) || !(/^[0-9]+$/).test(f2[0].value)) {
        window.alert("Пожалуйста, вводите только числа!");
        return false;
    }
    r.innerHTML = "Сумма " + (f1[0].value * f2[0].value);
    return false;
}

function updatePrice() {
    // Находим select по имени в DOM.
    let s = document.getElementsByName("prodType");
    let select = s[0];
    let price = 0;
    let prices = getPrices();
    let priceIndex = parseInt(select.value) - 1;
    if (priceIndex >= 0) {
        price = prices.prodTypes[priceIndex];
    }

    // Скрываем или показываем радиокнопки.
    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = (select.value == "3" ? "block" : "none");

    // Смотрим какая товарная опция выбрана.
    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function (radio) {
        if (radio.checked) {
            let optionPrice = prices.prodOptions[radio.value];
            if (optionPrice !== undefined) {
                price += optionPrice;
            }
        }
    });

    // Скрываем или показываем чекбоксы.
    let checkDiv = document.getElementById("checkboxes");
    checkDiv.style.display = (select.value == "4" ? "block" : "none");
    
    // Смотрим какие товарные свойства выбраны
    
    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function (checkbox) {
        if (checkbox.checked) {
            let propPrice = prices.prodProperties[checkbox.name];
            if (propPrice !== undefined) {
                price += propPrice;
            }
        }
    });
    let quanity = document.getElementsByName("quanity");
    quanity[0].value = quanity[0].value.replace(/[^0-9]/g, "");

    let prodPrice = document.getElementById("prodPrice");
    prodPrice.innerHTML = ((price)*(quanity[0].value)) + " рублей";
}


function getPrices() {
    return {
        prodTypes: [ 999, 3360, 4537],
        prodOptions: {
            option2: 980,
            option3: 1940,
        },
        prodProperties: {
            prop1: 990,
            prop2: 100,
        }
    };
}

window.addEventListener("DOMContentLoaded", function () {
    document.getElementById("quanity").onkeyup = updatePrice;
    
    let b = document.getElementById("button1");
    b.addEventListener("click", clicked);
    let r = document.getElementById("result");
    r.innerHTML = "Результат будет тут";

    /* ----------------------------------------------- */

    // Скрываем радиокнопки.
    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = "none";

    // Находим select по имени в DOM.
    let s = document.getElementsByName("prodType");
    let select = s[0];
    // Назначаем обработчик на изменение select.
    select.addEventListener("change", function (event) {
        let target = event.target;
        console.log(target.value);
        updatePrice();
    });

    // Назначаем обработчик радиокнопок.  
    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function (radio) {
        radio.addEventListener("change", function (event) {
            let r = event.target;
            console.log(r.value);
            updatePrice();
        });
    });

    // Назначаем обработчик радиокнопок.  
    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function (checkbox) {
        checkbox.addEventListener("change", function (event) {
            let c = event.target;
            console.log(c.name);
            console.log(c.value);
            updatePrice();
        });
    });

    updatePrice();
});
